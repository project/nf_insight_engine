<?php

namespace Drupal\nf_insight_engine;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Utils;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provide a integration with the insight engine endpoints.
 */
class InsightEngineApi {

  use StringTranslationTrait;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The http Client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;


  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   * */
  protected $currentRequest;

  /**
   * The serialization service.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $jsonSerialization;

  /**
   * The STAGING API Base url.
   */
  const STAGING_API_URL = 'https://stg.insight-engine.newfangled.com/api/v2';

  /**
   * LIVE API Base url.
   */
  const LIVE_API_URL = 'https://insight-engine.newfangled.com/api/v2';

  /**
   * The Insight Engine cookie name.
   */
  const COOKIE_NAME = 'nf_807d9';

  /**
   * Class construct function.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factoty service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack service.
   * @param \Drupal\Component\Serialization\SerializationInterface $json_serialization
   *   The json serialization service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, LoggerChannelFactoryInterface $logger_factory, RequestStack $request_stack, SerializationInterface $json_serialization) {
    $this->config = $config_factory->get('nf_insight_engine.settings');
    $this->httpClient = $http_client;
    $this->logger = $logger_factory->get('Insight engine');
    $this->currentRequest = $request_stack->getCurrentRequest();
    $this->jsonSerialization = $json_serialization;
  }

  /**
   * Tracks a piece of content into the insight engine.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $content_entity
   *   The content entity to track.
   */
  public function trackContentItem(ContentEntityInterface $content_entity) {

    $jsonParams = [
      'token' => $this->config->get('token'),
      'uniqueid' => $content_entity->id(),
      'url' => $content_entity->toUrl('canonical', ['absolute' => TRUE])->toString(),
      'title' => $content_entity->label(),
      'publishdate' => $content_entity->getCreatedTime(),
      'modifydate' => $content_entity->getChangedTime(),
      'author' => $content_entity->getOwner()->label(),
      'type' => $content_entity->bundle(),
    ];
    if ($this->config->get('debug_mode')) {
      $jsonParams['verbose'] = TRUE;
    }

    foreach ($this->config->get('tracked_entity_reference_fields') as $field) {
      if ($content_entity->hasField($field)) {
        foreach ($content_entity->{$field} as $reference) {
          $entity = $reference->entity;
          if ($entity) {
            $jsonParams['terms'][] = [$field, $entity->label()];
          }
        }
      }
    }

    $jsonParams = ['json' => $jsonParams];
    $response = $this->doPostCall('content', $jsonParams);
    if ($response) {
      if ($response->getStatusCode() == 200) {
        if ($this->config->get('debug_mode')) {
          $replacements = [
            '@content' => $content_entity->label(),
            '@json_response' => Utils::streamFor($response->getBody())->getContents(),
          ];
          $this->logger->notice($this->t("Registered @content into insight engine.\n Json response:\n @json_response .", $replacements));
        }
        return TRUE;
      }
      $this->logger->error('Something went wrong with the upsert content registration more details here: @details', ['@details' => Utils::streamFor($response->getBody())->getContents()]);
      return FALSE;
    }
    return FALSE;
  }

  /**
   * Registers Visitor convertion against the API.
   *
   * @param string $formName
   *   The Form name.
   * @param string $pageTitle
   *   The form Fields.
   * @param string $pageUrl
   *   The form host page URL.
   * @param array $formFields
   *   A array of form fields.
   * @param string $email
   *   (Optional) the email address to register against.
   */
  public function trackVisitorConversion(string $formName, string $pageTitle, string $pageUrl, array $formFields, string $email = NULL) {
    // Getting insight engine cookie.
    $sessionId = NULL;
    $cookieValue = $this->currentRequest->cookies->get(static::COOKIE_NAME);
    if ($cookieValue) {
      $sessionId = $this->jsonSerialization->decode($cookieValue);
    }

    $conversionData = [
      'token' => $this->config->get('token'),
      'session_id' => $sessionId,
      'form_name' => $formName,
      'page_url' => $pageUrl,
      'page_title' => $pageTitle,
    ];
    $conversionData['fields'] = $formFields;

    if ($email) {
      $conversionData['email'] = $email;
    }
    $response = $this->doPostCall('conversion', ['json' => $conversionData]);
    if ($response && $response->getStatusCode() == 200) {
      $newCookieValue = Utils::streamFor($response->getBody())->getContents();
      $this->logger->notice('Successed Conversion, response' . $newCookieValue);
      $this->currentRequest->cookies->set(static::COOKIE_NAME, $newCookieValue);
      return $response;
    }
  }

  /**
   * Handles POST request to the insight engine API.
   *
   * @param string $endpoint
   *   The ensight engine endpoint.
   * @param array $params
   *   A array of parameters to send throughout the POST.
   */
  private function doPostCall(string $endpoint, array $params) {
    try {
      return $this->httpClient->request('POST', $this->getApiUrl() . '/' . $endpoint, $params);
    }
    catch (GuzzleException $e) {
      $this->logger->error($e->getMessage());
      return FALSE;
    }
  }

  /**
   * Returns the base URL for the insight engine API.
   */
  private function getApiUrl() {
    if ($this->config->get('sandbox_mode')) {
      return static::STAGING_API_URL;
    }
    return static::LIVE_API_URL;
  }

  /**
   * Returns the Embed script url.
   */
  public function getEmbedScriptUrl() {
    return $this->getApiUrl() . '/' . $this->config->get('token') . '/include-api';
  }

}
