<?php

namespace Drupal\nf_insight_engine;

use Drupal\node\Entity\Node;

/**
 * Provides static methods for batch processing.
 */
class RegisterContentBatch {

  /**
   * Callback function to register your content into NF Insight Engine.
   *
   * @param int $nid
   *   The nodes ids to register into NF insight engine.
   * @param array $context
   *   The operation context.
   */
  public static function registerContent(int $nid, array &$context) {
    $node = Node::load($nid);
    if (!$node) {
      return;
    }
    $context['message'] = t('Registering @label into Insight engine', ['@label' => $node->label()]);
    $context['results'][$nid] = \Drupal::service('nf_insight_engine.api')->trackContentItem($node);

  }

  /**
   * Finnish callback for the Insight Engine registration.
   *
   * @param bool $success
   *   Flag to indicate if the process was a sucess.
   * @param array $results
   *   A array containing the ops results.
   * @param array $operations
   *   The operations array.
   */
  public static function registerFinishedCallback(bool $success, array $results, array $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = \Drupal::translation()->formatPlural(
        count($results),
        'One post registered with Insight Engine.',
        '@count posts registered with Insight Engine.'
      );
    }
    else {
      $message = t('Finished with an error.');
    }
    \Drupal::messenger()->addStatus($message);
  }

}
