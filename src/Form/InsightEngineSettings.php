<?php

namespace Drupal\nf_insight_engine\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a settings form insight engine.
 */
class InsightEngineSettings extends ConfigFormBase {

  /**
   * Config settings.
   */
  const SETTINGS = 'nf_insight_engine.settings';

  /**
   * The node Type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorage
   */
  protected $nodeTypeStotage;

  /**
   * The field config storage.
   *
   * @var \Drupal\field\FieldStorageConfigInterface
   */
  protected $fieldStorage;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->setConfigFactory($config_factory);
    $this->nodeTypeStotage = $entity_type_manager->getStorage('node_type');
    $this->fieldStorage = $entity_type_manager->getStorage('field_storage_config');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'insight_engine_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);
    $form['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable content tracking'),
      '#default_value' => $config->get('active'),
    ];

    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Insight Engine token'),
      '#default_value' => $config->get('token'),
    ];
    $sandboxReplacements = [
      '@staging' => 'https://stg.insight-engine.newfangled.com/api/v2',
      '@live' => 'https://insight-engine.newfangled.com/api/v2',
    ];
    $form['sandbox_mode'] = [
      '#type' => 'checkbox',
      '#description' => $this->t('When Enabled make usage of the staging API(@staging) instead of the live API (@live)', $sandboxReplacements),
      '#title' => $this->t('Sandbox mode'),
      '#default_value' => $config->get('sandbox_mode'),
    ];
    $contentTypesOptions = [];
    $contentTypes = $this->nodeTypeStotage->loadMultiple();
    foreach ($contentTypes as $contentType) {
      $contentTypesOptions[$contentType->id()] = $contentType->label();
    }
    $form['tracked_content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the content types you would like to track (Upsert Content)'),
      '#options' => $contentTypesOptions,
      '#default_value' => $config->get('tracked_content_types'),
    ];

    $reference_fields = $this->fieldStorage->loadByProperties([
      'type' => 'entity_reference',
      'entity_type' => 'node',
    ]);

    $fieds_options = [];
    foreach ($reference_fields as $field) {
      $fieds_options[$field->getName()] = $field->getName();
    }
    $form['tracked_entity_reference_fields'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the Entity reference fields you would like to track as terms'),
      '#options' => $fieds_options,
      '#default_value' => $config->get('tracked_entity_reference_fields'),
    ];

    $form['debug_mode'] = [
      '#type' => 'checkbox',
      '#description' => $this->t('Sends the "verbose" parameter to the insight Engine API'),
      '#title' => $this->t('Enables debug mode'),
      '#default_value' => $config->get('active'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(static::SETTINGS)
      ->set('token', $form_state->getValue('token'))
      ->set('tracked_content_types', $form_state->getValue('tracked_content_types'))
      ->set('tracked_entity_reference_fields', $form_state->getValue('tracked_entity_reference_fields'))
      ->set('active', $form_state->getValue('active'))
      ->set('debug_mode', $form_state->getValue('debug_mode'))
      ->set('sandbox_mode', $form_state->getValue('sandbox_mode'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
