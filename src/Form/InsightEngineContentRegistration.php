<?php

namespace Drupal\nf_insight_engine\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to register existing content into Insigh engine.
 */
class InsightEngineContentRegistration extends FormBase {

  /**
   * The node Type storage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorage
   */
  protected $nodeTypeStotage;

  /**
   * The database service.
   *
   * @var Drupal\Core\Database\Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Constructs for the content registry form.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $database) {
    $this->nodeTypeStotage = $entity_type_manager->getStorage('node_type');
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'insight_engine_content_registration';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $contentTypesOptions = [];
    $trackedContentTypes = $this->config('nf_insight_engine.settings')->get('tracked_content_types');
    $contentTypes = $this->nodeTypeStotage->loadMultiple();
    foreach ($contentTypes as $contentType) {
      if (in_array($contentType->id(), $trackedContentTypes)) {
        $contentTypesOptions[$contentType->id()] = $contentType->label();
      }
    }
    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the node types that would you like to register retroactively into Insight Engine'),
      '#description' => $this->t('Take all the nodes of the selected types and register them as content in Insight Engine.  Required for Insight Engine to properly track conversions across content types and taxonomies.'),
      '#options' => $contentTypesOptions,
    ];
    $form['register_content'] = [
      '#type' => 'submit',
      '#value' => $this->t('Register content'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selectedContentTypes = $form_state->getValue('content_types');
    foreach ($selectedContentTypes as $key => $value) {
      if ($value == 0) {
        unset($selectedContentTypes[$key]);
      }
    }
    $selectedContentTypes = array_keys($selectedContentTypes);
    if ($selectedContentTypes) {
      $query = $this->database->select('node', 'n')
        ->fields('n', ['nid'])
        ->condition('type', $selectedContentTypes, 'IN');
      $result = $query->execute();
      $result = array_values($result->fetchCol());
    }
    $opts = [];
    foreach ($result as $nid) {
      $opts[] = [
        'Drupal\nf_insight_engine\RegisterContentBatch::registerContent', [$nid],
      ];
    }

    $batch = [
      'title' => $this->t('Registering content into Insight Engine...'),
      'operations' => $opts,
      'finished' => 'Drupal\nf_insight_engine\RegisterContentBatch::registerFinishedCallback',
    ];

    batch_set($batch);
  }

}
