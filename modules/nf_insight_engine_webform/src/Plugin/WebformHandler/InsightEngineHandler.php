<?php

namespace Drupal\nf_insight_engine_webform\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteObjectInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "insight_engine",
 *   label = @Translation("Insight engine conversion"),
 *   category = @Translation("Statistics"),
 *   description = @Translation("Sends conversion data to insight engine"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class InsightEngineHandler extends WebformHandlerBase {

  /**
   * The insight Engine service.
   *
   * @var \Drupal\nf_insight_engine\InsightEngineApi
   */
  protected $insightEngineApi;

  /**
   * The title resolver service.
   *
   * @var \Drupal\Core\Controller\TitleResolverInterface
   */
  protected $titleResolver;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    $instance->loggerFactory = $container->get('logger.factory');
    $instance->insightEngineApi = $container->get('nf_insight_engine.api');
    $instance->request = $container->get('request_stack')->getCurrentRequest();
    $instance->titleResolver = $container->get('title_resolver');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [] + parent::getSummary();
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'tracked_fields' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $webFormFields = $this->webform->getElementsDecodedAndFlattened();
    $fieldsOptions = [];
    foreach ($webFormFields as $key => $field) {
      $fieldsOptions[$key] = $field['#title'];
    }

    $form['field_tracking_options'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Field tracking options'),
    ];
    $form['field_tracking_options']['tracked_fields'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the fields you would like to track for the conversion'),
      '#description' => $this->t('You should have at least one email field selected Named Email, mail or something along those lines to allow ig to identify your conversion as valid'),
      '#options' => $fieldsOptions,
      '#default_value' => $this->configuration['tracked_fields'],
      '#required' => TRUE,
    ];

    $this->elementTokenValidate($form);
    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {
    $trackedFields = $this->configuration['tracked_fields'];
    $enFields = [];
    foreach ($trackedFields as $key => $field) {
      $enFields[$field] = $form_state->getValue($key);
    }
    $formName = $this->webform->label();
    $route = $this->request->attributes->get(RouteObjectInterface::ROUTE_OBJECT);
    $pageTitle = $this->titleResolver->getTitle($this->request, $route);
    $pageUrl = $webform_submission->getSourceUrl()->toString();
    $this->insightEngineApi->trackVisitorConversion($formName, $pageTitle, $pageUrl, $enFields);
  }

}
